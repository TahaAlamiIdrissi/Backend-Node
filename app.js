const app = require("express")();
const mongoose = require("mongoose");
const bodyParser = require('body-parser');
require("dotenv/config");

//importing routes
const userRoutes = require('./api/routes/users');
const contentRoutes = require('./api/routes/content');
const postRoutes = require('./api/routes/posts');

mongoose.connect(process.env.DB_CONNECT, { useNewUrlParser: true }, () =>
  console.log("Connected to DB")
);
app.use(bodyParser.json());
app.use('/api/users',userRoutes);   
app.use('/api/contents',contentRoutes);
app.use('/api/posts',postRoutes);

app.listen(process.env.PORT, () => console.log("Listening on PORT 3000"));
