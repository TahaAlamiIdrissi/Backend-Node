const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema({
  _id: Schema.Types.ObjectId,
  firstName: {
    type: String,
    required: true,
    max: 255
  },
  lastName: {
    type: String,
    required: true,
    max: 255
  },
  email: {
    type: String,
    required: true,
    max: 255
  },
  password: {
    type: String,
    required: true,
    max: 50
  },
  posted: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  created_at: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("user", userSchema);
