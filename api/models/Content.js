const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const contentSchema = new Schema({
  _id: Schema.Types.ObjectId,
  link: {
    type: String,
    required: true,
    max: 255
  },
  posted: {
    type: Number
  },
  created_at: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("content", contentSchema);
