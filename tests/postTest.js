const supertest = require("supertest");
const should = require("should");
// agent refering to PORT where program is running

const request = supertest.agent("http://localhost:3000");

//Unit test posting a new post

describe("POST /api/posts/ #create a new post", () => {
  // it should return a status 200 with a json message {message:'xxxx'}
  it("should return parsed json message with 200 status", done => {
    request
      .post("/api/posts/")
      .expect("Content-type", /json/)
      .expect(200)
      .end((err, res) => {
        // HTTP status should be 200
        res.status.should.equal(200);
        done();
      });
  });
});
