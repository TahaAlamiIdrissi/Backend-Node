const jwt = require("jsonwebtoken");

module.exports = (req, res, next) => {
  if (!req.headers.authorization)
    return res.status("403").send({ message: "Acces Denied" });
  const token = req.headers.authorization.split(" ")[1];
  if (!token) return res.status("403").send({ message: "Acces Denied" });

  try {
    const verified = jwt.verify(token, process.env.TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (error) {
    res.status(400).send({ error });
  }
};
