const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const postSchema = new Schema({
  _id: Schema.Types.ObjectId,
  title: {
    type: String,
    required: true,
    max: 255
  },
  description: {
    type: String,
    required: true,
    max: 255
  },
  like: {
    type: Number
  },
  dislike: {
    type: Number
  },
  body:{
    type: String
  },
  comment: [{
      body: String,
      date: Date
    }],
  content: { type: Schema.Types.ObjectId, ref: 'Content' },
  created_at: {
    type: Date,
    default: Date.now()
  }
});

module.exports = mongoose.model("post", postSchema);
