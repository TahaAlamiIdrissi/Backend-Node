const Joi = require("@hapi/joi");

const registerValidation = data => {
  const Schema = {
    firstName: Joi.string()
      .required()
      .max(255),
    lastName: Joi.string()
      .required(true)
      .max(255),
    email: Joi.string()
      .required(true)
      .max(255)
      .email(),
    password: Joi.string()
      .required(true)
      .max(255)
  };
  return Joi.validate(data, Schema);
};
const loginValidation = data => {
  const Schema = {
    email: Joi.string()
      .required(true)
      .max(255)
      .email(),
    password: Joi.string()
      .required(true)
      .max(255)
  };
  return Joi.validate(data, Schema);
};
const contentValidation = data => {
  const Schema = {
    link: Joi.string()
      .required()
      .max(255)
  };
  return Joi.validate(data, Schema);
};
const postValidation = data => {
  const Schema = {
    title: Joi.string()
      .max(255)
      .required(true),
    description: Joi.string()
      .required(true)
      .max(255),
    body: Joi.string()
      .max(255)
      .required(true)
  };
  return Joi.validate(data,Schema);
};
module.exports = {
  registerValidation,
  loginValidation,
  contentValidation,
  postValidation
};
