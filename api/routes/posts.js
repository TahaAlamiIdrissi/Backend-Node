const router = require("express").Router();
const mongoose = require("mongoose");
const { postValidation } = require("../../validation");
const Post = require('../models/Post');
//  posts API , to create and share posts , like them , comment posts

/*
 *   url : /api/posts/test
 *   goal: testing route
 *   privacy: public
 */

router.get("/test", (req, res) => {
  res.send("Posts testing route");
});

/*
 *   url : /api/posts/
 *   goal: creating a new post
 *   privacy: private(should be logged in )
 */

router.post("/",async (req, res) => {
  //validate the data
  const { error } = contentValidation(req.body);
  if (error) return res.status(400).json({ message: error.details[0].message });
  //save the post
    const post = await new Post({
        _id: mongoose.Types.ObjectId(),
        title: req.body.title,
        description: req.body.description,
    });
  try {
    res.status(200).send({ message: "Content saved successfully" });
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
