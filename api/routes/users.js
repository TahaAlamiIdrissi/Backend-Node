const router = require("express").Router();
const User = require("../models/User");
const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const verify = require('../../verifyToken');
const { registerValidation, loginValidation } = require("../../validation");

//  User Autentication API with login and register

/*
 *   url : /api/users/test
 *   goal: testing route
 *   privacy: public
 */

router.get("/test",verify, (req, res) => {
  res.send("Testing route");
});

/*
 *   url : /api/users/register
 *   goal: registering route
 *   privacy: public
 */

router.post("/register", async (req, res) => {
  //validate the data
  const { error } = registerValidation(req.body);
  if (error) return res.status(500).json({ message: error.details[0].message });
  //check if user is already present in the database
  const emailExist = await User.findOne({ email: req.body.email });
  if (emailExist)
    return res.status(500).json({ message: "Email Already Exist !" });
  // hash the password
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(req.body.password, salt);
  //save the user
  const user = new User({
    _id: mongoose.Types.ObjectId(),
    firstName: req.body.firstName,
    lastName: req.body.lastName,
    email: req.body.email,
    password: hashedPassword
  });

  const savedUser = await user.save();

  try {
    res
      .status(200)
      .send({ message: `${req.body.lastName} has been saved succefully ! ` });
  } catch (error) {
    res.status(500).send(error);
  }
});

/*
 *   url : /api/users/login
 *   goal: login in  route
 *   privacy: public
 */

router.post("/login", async (req, res) => {
  //validate the data
  const { error } = loginValidation(req.body);
  if (error) return res.status(500).json({ message: error.details[0].message });
  // check if the user is present in the DB
  const user = await User.findOne({ email: req.body.email });
  if (!user) return res.status(500).json({ message: "Invalid Email !" });
  // compare hashed password
  const isPassCorrect = await bcrypt.compare(req.body.password, user.password);
  if (!isPassCorrect)
    return res.status(500).json({ message: "Invalid Password ! " });
  // create and assign token
  const token = jwt.sign({ _id: user._id }, process.env.TOKEN_SECRET);
  res.header('authorization',token);
  res.send('Done !');
});

module.exports = router;
