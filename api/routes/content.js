const router = require("express").Router();
const mongoose = require("mongoose");
const { contentValidation } = require("../../validation");
const Content = require("../models/Content");
const verify = require("../../verifyToken");

//  content API create a content , get all contents , update a content , delete a content

/*
 *   url : /api/contents/test
 *   goal: testing route
 *   privacy: public
 */

router.get("/test", (req, res) => {
  res.send("Content testing route");
});

/*
 *   url : /api/contents/
 *   goal: create a content
 *   privacy: private
 */
router.post("/", async (req, res) => {
  //validate the data
  const { error } = contentValidation(req.body);
  if (error) return res.status(400).send({ message: error.details[0].message });
  //save the content
  const content = new Content({
    _id: mongoose.Types.ObjectId(),
    link: req.body.link
  });
  const savedContent = await content.save();
  try {
    res.status(200).send({ message: "Content saved successfully" });
  } catch (error) {
    res.send(error);
  }
});

/*
 *   url : /api/contents/
 *   goal: get all contents
 *   privacy: private
 */

router.get("/", async (req, res) => {
  const contents = await Content.find();
  try {
    res.status(200).json({ contents });
  } catch (error) {
    res.send(error);
  }
});

/*
 *   url : /api/contents/:id
 *   goal: get a content by its ID
 *   privacy: private
 */
router.get("/:contentId", async (req, res) => {
  const contentId = req.params.contentId;

  const content = await Content.findById(contentId);
  try {
    res.status(200).json({ content });
  } catch (error) {
    res.send(error);
  }
});

module.exports = router;
